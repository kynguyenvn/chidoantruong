import React from 'react'
export const Footer = () => (
  <div>
    <div className="footer-section pad-40">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h2 className="ft-title"><span>Liên lạc với chúng tôi</span></h2>
            <div className="footer-widget-1 address">
              <ul>
                <li><i className="fa fa-home"></i>Tổ 3 - Phường Hoà Khánh Nam - Quận Liên Chiểu - TP Đà Nẵng </li>
                <li><i className="fa fa-envelope"></i><a href="mailto:ithemeslab@gmail.com">thcsngothinhamdn@gmail.com</a></li>
                <li><i className="fa fa-phone"></i>+84 (511)  3737619   </li>
                <li><i className="fa fa-globe"></i><a href="http://ithemeslab.com" target="_blank"> http://thcsngothinham.edu.vn/</a></li>
              </ul>
            </div>
          </div>
          <div className="col-md-6 ">
            <div className="footer-widget-2 text-center">
              <img src={ require('../assets/footer-logo.png') } alt="footer-logo"/>
              <p>Đã trót sinh ra trong trời đất,Phải có danh gì với núi sông</p>
            </div>
            <div className="footer-social text-center">
              <div className="social-icon offset-md-2">
                <ul>
                  <li style={{ paddingLeft:'120' }}>
                    <a title="Facebook"><i className="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a title="Google plus"><i className="fa fa-google-plus"></i></a>
                  </li>
                  <li>
                    <a title="YouTube"><i className="fa fa-youtube"></i></a>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Footer
