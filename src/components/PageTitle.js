import React from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

export const PageTitle = ({ title }) => (
  <div className='row page-titles'>
    <div className='col-md-5 align-self-center'>
      <h4 className='text-themecolor'>{title}</h4>
    </div>
    <div className='col-md-7 align-self-center text-right'>
      <div className='d-flex justify-content-end align-items-center'>
        <ol className='breadcrumb'>
          <li className='breadcrumb-item'><a href='javascript:void(0)'>Home</a></li>
          <li className='breadcrumb-item active'>{title}</li>
        </ol>
        <Link to='/real-estate-add' className='btn btn-info d-none d-lg-block m-l-15'>
          <i className='fa fa-plus-circle' style={{ paddingRight: 10 }} />
          Create New
        </Link>
      </div>
    </div>
  </div>

)
PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
}
export default PageTitle
