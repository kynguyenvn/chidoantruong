import React from 'react'
import OwlCarousel from 'react-owl-carousel'

export const Carousel = () => (
  <OwlCarousel
    className='owl-theme'
    items={1}
    autoplay
    autoplayTimeout={2000}
    dots={false}
    loop margin={10}
    >
    {
        [1, 2, 3].map((item, index) => {
          return (<div key={index}>
            <div>
              <img src={require(`../components/assets/slider${item}.jpg`)} alt={`Slider ${item}`} />
            </div>
            <div className='slide-caption'>
              <h2 className='slide3anim1'>Chi Đoàn Thcs <span>Ngô Thì Nhậm</span></h2>
              <h3 className='slide3anim2'>Đừng bao giờ hỏi chi đoàn có thể làm gì cho chúng ta<br /> nhưng hãy tự hỏi chúng ta có thể làm gì cho chi đoàn</h3>
              <a href='#' className='btn btn-slideshow slide1anim3'>Tham Gia Ngay</a>
            </div>
          </div>
          )
        })
      }
  </OwlCarousel>
)

export default Carousel
