import React, { Component } from 'react'
import OwlCarousel from 'react-owl-carousel'
import PropTypes from 'prop-types'

class Carousel1 extends Component {
  render () {
    let { aboutUsCurrent } = this.props
    return (
      <div>
        <OwlCarousel
          className='owl-theme'
          ref='car'
          items={1}
          autoplay
          autoplayTimeout={5000}
          loop
          margin={10}

        >
          {
            aboutUsCurrent.map((item, index) => {
              return (<div key={index} className='owl-item' style={{ width: '555px', marginRight: '0px' }}>
                {

                  <div className='item'>
                    <img style={{ height:350, width:'100%' }} src={item.images[0].url} alt='slider4' />
                  </div>

                  }

              </div>
              )
            })
          }

        </OwlCarousel>
        <div className='text-center'>
          <button onClick={() => this.refs.car.prev()}>
            <i className='fa fa-angle-left' />
          </button>

          <button onClick={() => this.refs.car.next()}>
            <i className='fa fa-angle-right' />
          </button>
        </div>
      </div>

    )
  }
}

Carousel1.propTypes = {
  aboutusCurrent: PropTypes.array.isRequired,
}
export default Carousel1
