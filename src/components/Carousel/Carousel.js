import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import OwlCarousel from 'react-owl-carousel'

export const Carousel = ({ products }) => (
  <OwlCarousel
    className='owl-theme'
    items={1}
    autoplay
    autoplayTimeout={5000}
    dots={false}
    loop margin={10}
    >
    {
        products.map((item, index) => {
          return (<div key={index} className=''>
            <img src={item.url} alt={item} style={{height: '440px'}} />
          </div>)
        })
      }
  </OwlCarousel>
)
Carousel.propTypes = {
  products: PropTypes.array.isRequired,
}
export default Carousel
