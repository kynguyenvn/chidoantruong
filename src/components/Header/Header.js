import React from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

export const Header = ({isLogin, logout}) => (
  <div>
    <section>
      <div className='top-header hidden-xs'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-6'>
              <div className='social-icon'>
                <ul>
                  <li>
                    <a title='Facebook'><i className='fa fa-facebook' /></a>
                  </li>
                  <li>
                    <a title='Google plus'><i className='fa fa-google-plus' /></a>
                  </li>
                  <li>
                    <a title='YouTube'><i className='fa fa-youtube' /></a>
                  </li>
                </ul>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='top-email pull-right'>
                  <i className='fa fa-envelope-o' /> ngothinham@chidoan.com
              </div>
              <div className='top-phone pull-right'>
                <i className='fa fa-phone' /> +84 123123789
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <header className='header' id='header' role='banner'>
        <div className='navbar'>
          <div className='container'>
            <div className='navbar-header'>
              <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='.navbar-collapse'>
                <span className='sr-only'>Toggle navigation</span>
                <span className='icon-bar' />
                <span className='icon-bar' />
                <span className='icon-bar' />
              </button>
              <Link className='navbar-brand' to={'/'} >
                <img className='img-responsive' src={ require('../assets/logo.png') } alt='' />
              </Link>
            </div>
            <nav className='collapse navbar-collapse'>
              <ul className='nav navbar-nav navbar-right'>
                <Link to='/home' activeClassName='active'>Trang Chủ</Link>
                <Link activeClassName='active' to='/about-us'>Về Chi Đoàn</Link>
                <Link activeClassName='active' to='/gallery'>Thư Viện Ảnh</Link>
                {
                  isLogin
                  && <Link activeClassName='active' to ='/list-album'>Danh Sách Ảnh</Link>

                }
                {
                  isLogin
                  ? <Link activeClassName='active' to='/add-album'>Thêm ảnh</Link>
                    :  <Link activeClassName='active' to='/login'>Đăng nhập</Link>

                }
                {
                  isLogin
                  && <Link activeClassName='active' onClick={() => logout()}>Đăng xuất</Link>

                }

              </ul>
            </nav>
          </div>
        </div>
      </header>
    </section>
  </div>
)
Header.propTypes = {
  isLogin: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
}
export default Header
