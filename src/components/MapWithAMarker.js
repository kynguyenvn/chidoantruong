import _ from 'lodash'
import React from 'react'
import { compose, withProps, lifecycle } from 'recompose'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Circle
} from 'react-google-maps'

const MyMapComponent = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyBbW7tk3RbIQmucr-iTmjt5kxVVOlC2lX0&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `300px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  lifecycle({
    componentWillMount () {
      // console.log(this)
      const refs = {}

      this.setState({
        bounds: null,
        center: {
          lat: 16.060950, lng: 108.222190,
        },
        markers: [],
        onMapMounted: ref => {
          refs.map = ref
        },
        onBoundsChanged: () => {
          this.setState({
            bounds: refs.map.getBounds(),
            center: refs.map.getCenter(),
          })
        },
        onSearchBoxMounted: ref => {
          refs.searchBox = ref
        },
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces()
          const bounds = new google.maps.LatLngBounds()

          places.forEach(place => {
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport)
            } else {
              bounds.extend(place.geometry.location)
            }
          })
          const nextMarkers = places.map(place => ({
            position: place.geometry.location,
          }))
          const nextCenter = _.get(nextMarkers, '0.position', this.state.center)
          const location = {
            lat: nextCenter.lat(),
            lng: nextCenter.lng()
          }
          this.props.onChangeLocation(places, location)

          this.setState({
            center: nextCenter,
            markers: nextMarkers,
          })
          // refs.map.fitBounds(bounds);
        },
      })
    },
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={15}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
  >
    <Marker
      position={props.center}
     />
    <Circle
      center={props.center}
      radius={100}
      ref={circle => { this.circle = circle }}
      // onCenterChanged={this.onCenterChanged}
      // onRadiusChanged={this.onRadiusChanged}
      options={{
        fillColor: '#f00',
        strokeColor: '#f00',
        strokeOpacity: 1.0,
        strokeWeight: 1.0,
        scale: 1.0
      }}
    />
  </GoogleMap>
))

const enhance = _.identity

const ReactGoogleMaps = (onChangeLocation) => (
  <MyMapComponent key='map' onChangeLocation={onChangeLocation.onChangeLocation} />
)

export default enhance(ReactGoogleMaps)
