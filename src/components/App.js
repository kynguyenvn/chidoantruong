import React from 'react'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'
import firebase from 'firebase'
const config = {
  apiKey: 'AIzaSyAOJy826Ae8WA_ZKHzWxZCEX5n9sli0xFQ',
  authDomain: 'social-ad894.firebaseapp.com',
  databaseURL: 'https://social-ad894.firebaseio.com',
  projectId: 'social-ad894',
  storageBucket: 'gs://social-ad894.appspot.com/',
  messagingSenderId: '433140247769'
}
firebase.initializeApp(config)
class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: ''
    }
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <Router history={browserHistory} children={this.props.routes} />
      </Provider>
    )
  }
}

export default App
