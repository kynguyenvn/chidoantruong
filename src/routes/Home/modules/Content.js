// Constants
import firebase from 'firebase/index'

export const FETCH_GALLERY_SUCCESS = 'FETCH_GALLERY_SUCCESS'
export const FETCH_ABOUTUS_SUCCESS = 'FETCH_ABOUTUS_SUCCESS'
export const FETCH_PRAISE_SUCCESS = 'FETCH_PRAISE_SUCCESS'

// Actions

const fetchGallerySuccess = (listGallery) => ({
  type    : FETCH_GALLERY_SUCCESS,
  listGallery
})
const fetchAboutusSuccess = (listAboutus) => ({
  type    : FETCH_ABOUTUS_SUCCESS,
  listAboutus
})
const fetchPraiseSuccess = (listPraise) => {
  console.log(1)
  return ({
    type: FETCH_PRAISE_SUCCESS,
    listPraise
  })
}

export const fetchGallery = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/gallery/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        if (cb)cb(Object.values(data))
        dispatch(fetchGallerySuccess(Object.values(data)))
      })
    })
  }
}
export const fetchAboutus = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/aboutUs/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        if (cb)cb(Object.values(data))
        dispatch(fetchAboutusSuccess(Object.values(data)))
      })
    })
  }
}
export const fetchPraise = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/praise/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        if (cb)cb(Object.values(data))
        dispatch(fetchPraiseSuccess(Object.values(data)))
      })
    })
  }
}
export const actions = {
  fetchGallery, fetchAboutus, fetchPraise
}

// Action Handlers

const ACTION_HANDLERS = {
  [FETCH_GALLERY_SUCCESS] : (state, action) => ({ ...state, listGallery: action.listGallery }),
  [FETCH_ABOUTUS_SUCCESS] : (state, action) => ({ ...state, listAboutus: action.listAboutus }),
  [FETCH_PRAISE_SUCCESS] : (state, action) => ({ ...state, listPraise: action.listPraise }),
}

// Reducer

const initialState = {
  listGallery: [],
  listAboutus:[],
  listPraise: []
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
