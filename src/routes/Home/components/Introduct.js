import React, { Component } from 'react'
export class Introduct extends Component {
  render () {
    const { aboutUs } = this.props
    return (
      <section id='extensions' className='extensions pad-80'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-8 col-sm-8 col-xs-12'>
              <h2 className='lg-title-2'>Thông tin về chi đoàn</h2>
              <p>Chi đoàn trung học cơ sở Ngô Thì Nhậm với lý tưởng </p>

              <div className='row'>
                <div className='col-md-6'>
                  <div className=''>
                    <ul className='fa-ul'>
                      <li><i className='fa-li fa fa-joomla' /> thi đua học tập, rèn luyện, lao động, sáng tạo</li>
                      <li><i className='fa-li fa fa-image' /> làm theo tư tưởng, tấm gương đạo đức, phong cách Hồ Chí Minh</li>
                      <li><i className='fa-li fa fa-gears' /> xây hoài bão lớn, vững tin, kiến quốc, lập nghiệp</li>
                    </ul>
                  </div>
                </div>
                <div className='col-md-6'>
                  <div className=''>
                    <ul className='fa-ul'>
                      <li><i className='fa-li fa fa-leaf' />  xung kích, tình nguyện vì cộng đồng</li>
                      <li><i className='fa-li fa fa-file' /> xây dựng nguồn nhân lực trẻ</li>
                      <li><i className='fa-li fa fa-umbrella' />  đồng hành với thanh niên trong phát triển kỹ năng xã hội</li>
                    </ul>
                  </div>
                </div>
              </div>

              <p> Thành lập năm 1977 với tên gọi ban đầu là trường vừa học vừa làm Thanh Trì, trải qua 40 năm xây dựng và trưởng thành thầy và trò nhà trường đã đạt được những kết quả đáng khích lệ: 38 năm liên tục đạt danh hiệu tập thể lao động Tiên tiến và Tập thể lao động Xuất sắc; Chi bộ trong sạch vững mạnh và vững mạnh tiêu biểu; Công đoàn liên tục được công nhận là Công đoàn vững mạnh và vững mạnh xuất sắc; Đoàn thanh niên nhiều năm được Trung ương đoàn và Thành đoàn tặng Bằng khen.</p>
            </div>
            <div className='col-md-4 col-sm-4 col-xs-12'>
              <div className='volunteer text-center'>
                {
                  (aboutUs.images ? aboutUs.images.map((item, index) =>
                    <div key={index}>
                      <img className='img-responsive' src={item.url} alt='volunteer' />
                      <h2>Tình Nguyện Viên</h2>
                      <h3>Hãy đến với chúng tôi</h3>
                      <p>Hãy đến và thể hiện sức trẻ của bạn</p>
                      <a className='btn btn-join' href='#'>Tham gia nhé</a>
                    </div>
                    ) : <div> loading...</div>
                  )
                }

              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
