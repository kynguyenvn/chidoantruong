import React, { Component } from 'react'
import { Link } from 'react-router'
export class Album extends Component {
  render () {
    const { gallery, galleriesCurrent } = this.props
    // console.log(galleriesCurrent, 'galleriesCurrent')
    return (
      <div>
        <section id='causes-header' className='causes-header'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-12 col-sm-12 col-xs-12'>
                <h2 className='lg-title text-center'><span>Ảnh Hoạt Động</span></h2>
              </div>
              <div className='col-md-6 col-sm-12 col-xs-12 col-md-offset-3' />
            </div>
          </div>
        </section>
        <section id='causes' className='causes'>
          <div className='container'>
            <div className='row text-center'>
              <div className='isotope-nav' />
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='row'>
                  <div className='urgent-causes' style={{ position: 'relative', height: '491.5px' }}>
                    <div className='row'>
                      {
                        galleriesCurrent && galleriesCurrent.map((item, index) => (
                          <div key={index} className='col-6 col-md-4' >
                            <div className='grid'>
                              <figure className='effect-oscar'>
                                <img src={item.images[0].url} alt='ug-1' style={{ height:'200', width:'300' }} />
                                <figcaption>
                                  {
                                    (
                                      <div>
                                        <Link to={{ pathname: `/detail-album/${item.id}`, state:{ albumData: item } }}>
                                          <a href='#' className='btn btn-causes'>Xem chi
                                          tiết</a></Link>
                                      </div>

                                    )
                                  }
                                </figcaption>
                              </figure>
                            </div>
                            <div className='text-center'>
                              <h3 className='text-success'>{item.name}</h3>
                            </div>
                          </div>
                        ))
                      }

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='row text-center'>
              <div className='col-md-12 col-sm-12 col-xs-12'>
                <Link to='/gallery'><a href='#' className='btn btn-loadmore'>Xem Thêm</a></Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
