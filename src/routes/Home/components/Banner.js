import React, { Component } from 'react'
import Carousel from '../../../components/Carousel'

export class Banner extends Component {
  render () {
    return (
      <section>
        <section id='slideshow' className='slideshow'>
          <div id='main-slideshow' className='main-slideshow owl-carousel owl-theme owl-loaded'>
            <div className='owl-stage-outer'>
              <div className='owl-stage'>
                <div className='owl-item cloned' style={{ width: '1286px', marginRight: '0px' }}>
                  <div className='item'>
                    <Carousel />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    )
  }
}
