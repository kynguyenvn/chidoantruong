import React, { Component } from 'react'
import Carousel from '../../../components/Carousel'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { WhatDoWeCan } from './WhatDoWeCan'
import { Introduct } from './Introduct'
import { Event } from './Event'
import { Album } from './Album'
import { Banner } from './Banner'

class ContentView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      galleriesCurrent: [],
      gallery: {},
      arrayAlbum:[],
      aboutusCurrent: [],
      aboutUs : {},
      praiseCurrent:[],
      praise:{}
    }
    this.onChangeAlbumCurrent = this.onChangeAlbumCurrent.bind(this)
  }
  componentWillMount () {
    // fetch gallery
    this.props.fetchGallery((galleries) => {
      // console.log(this.handleGalleries(galleries))
      const galleriesCurrent = this.handleGalleries(galleries)
      galleriesCurrent.sort(function compare (a, b) {
        const dateA = new Date(a.dateCreate)
        const dateB = new Date(b.dateCreate)
        return dateB - dateA
      })
      this.setState({
        galleriesCurrent,
        gallery: galleriesCurrent[0]
      })
    })
    // fetch Aboutus
    this.props.fetchAboutus((aboutuss) => {
      // console.log(this.handleGalleries(galleries))
      const aboutusCurrent = this.handleAboutus(aboutuss)
      console.log(aboutusCurrent, 'aboutusCurrent')
      aboutusCurrent.sort(function compare (a, b) {
        const dateA = new Date(a.dateCreate)
        const dateB = new Date(b.dateCreate)
        return dateB - dateA
      })
      this.setState({
        aboutusCurrent,
        aboutUs: aboutusCurrent[0]
      })
    })
    // fetch Praise
    this.props.fetchPraise((praisee) => {
      // console.log(this.handleGalleries(galleries))
      const praiseCurrent = this.handleAboutus(praisee)
      praiseCurrent.sort(function compare (a, b) {
        const dateA = new Date(a.dateCreate)
        const dateB = new Date(b.dateCreate)
        return dateB - dateA
      })
      this.setState({
        praiseCurrent,
        aboutUs: praiseCurrent[0]
      })
    })
  }
  handleGalleries (galleries) {
    const lengthGallery = galleries.length
    let array = []
    for (let i = 0; i < lengthGallery; i++) {
      if (i > lengthGallery - 7) {
        array.push(galleries[i])
      }
    }
    return array
  }
  handleAboutus (aboutuss) {
    const lengthAboutus = aboutuss.length
    let array = []
    for (let i = 0; i < lengthAboutus; i++) {
      if (i > lengthAboutus - 5) {
        array.push(aboutuss[i])
      }
    }
    return array
  }
  handlePraise (praisee) {
    const lengthPraise = praisee.lengh
    let array = []
    for (let i = 0; i < lengthPraise; i++) {
      if (i > lengthPraise - 5) {
        array.push(praisee[i])
      }
    }
    return array
  }
  onChangeAlbumCurrent (gallery) {
    this.setState({ gallery })
  }
  render () {
    const { galleriesCurrent, gallery, aboutUs, aboutusCurrent, praiseCurrent } = this.state
    return (
      <div>
        <Banner />
        <WhatDoWeCan />
        <Album gallery={gallery} galleriesCurrent={galleriesCurrent} onChangeAlbumCurrent={(item) => this.onChangeAlbumCurrent(item)} />
        <Introduct aboutUs={aboutUs} />
        <Event praiseCurrent={praiseCurrent} />
      </div>
    )
  }
 }
ContentView.propTypes = {
  fetchGallery: PropTypes.func.isRequired,
  listGallery: PropTypes.array.isRequired,
  fetchAboutus: PropTypes.func.isRequired,
  listAboutus: PropTypes.array.isRequired,
  fetchPraise: PropTypes.func.isRequired,
  listPraise: PropTypes.array.isRequired,
}
export default ContentView
