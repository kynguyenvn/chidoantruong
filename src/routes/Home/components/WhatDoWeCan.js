import React, { Component } from 'react'
export class WhatDoWeCan extends Component {
  render () {
    return (
      <div>
        <section id='feature-header' className='feature-header'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-12 col-sm-12 col-xs-12'>
                <h2 className='lg-title text-center'><span>Chúng tôi có thể làm gì cho bạn!</span></h2>
              </div>
              <div className='col-md-8 col-sm-12 col-xs-12 col-md-offset-2'>
                <p className='lg-text text-center'>Yêu thương chỉ lan tỏa qua chia sẻ. Bạn chỉ có thể có nhiều yêu thương hơn cho mình bằng cách trao đi yêu thương.
                  <br /> Love only grows by sharing. You can only have more for yourself by giving it away to others.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id='feature' className='feature pad-80'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-3 col-sm-3 col-xs-12'>
                <div className='feature-wrap'>
                  <div className='icon-wrap'><i className='fa fa-child' /></div>
                  <h3>Tình Nguyện</h3>
                  <p>Hãy tham gia “Hội tình nguyện”: tham gia để chia sẻ - tham gia để cống hiến - tham gia để khẳng định - tham gia để trưởng thành</p>
                </div>
              </div>
              <div className='col-md-3 col-sm-3 col-xs-12'>
                <div className='feature-wrap'>
                  <div className='icon-wrap'><i className='fa fa-globe' /></div>
                  <h3>Chia Sẽ</h3>
                  <p>Mọi người hãy dành cho nhau tình yêu thương và sự chia sẻ để bản thân mình và những người xung quanh cảm nhận được hạnh phúc đích thực trong cuộc sống</p>
                </div>
              </div>
              <div className='col-md-3 col-sm-3 col-xs-12'>
                <div className='feature-wrap'>
                  <div className='icon-wrap'><i className='fa fa-user' /></div>
                  <h3>Tổ Chức</h3>
                  <p>Để tăng cường sự gắn bó, tình đoàn kết giữa các đoàn viên, BCH chi đoàn đã chủ động xây dựng kế hoạch hoạt động theo từng tháng, phân công nhiệm vụ cụ thể cho từng uỷ viên BCH,Và tổ chức các hoạt động khác</p>
                </div>
              </div>
              <div className='col-md-3 col-sm-3 col-xs-12'>
                <div className='feature-wrap'>
                  <div className='icon-wrap'><i className='fa fa-umbrella' /></div>
                  <h3>Hỗ Trợ</h3>
                  <p>Chi đoàn hỗ trợ giúp đỡ các thành viên học sinh trong trường về các mặt hoạt động ngoại khóa ,học tập </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id='current-cause' className='current-cause pad-80 parallax pbg1'
          style={{ backgroundImage: `url(${require('../../../components/assets/parallax-1.jpg')})` }}>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-sm-12 col-xs-12 col-md-offset-2'>
                <h2 className='lg-title-2 white-text text-center'>xây dựng chi đoàn vững mạnh</h2>
              </div>
              <div className='col-md-8 col-sm-12 col-xs-12 col-md-offset-2'>
                <p className='lg-text text-center white-text'>Chi đoàn là tổ chức tế bào của Đoàn, là hạt nhân nòng cốt đoàn kết, tập hợp thanh thiếu nhi. Việc xây dựng chi đoàn vững mạnh là giải pháp quan trọng để củng cố, xây dựng cơ sở đoàn vững mạnh, củng cố và nâng cao chất lượng tổ chức
                </p>
              </div>
              <div className='col-md-12 col-sm-12 col-xs-12 pad-30'>
                <div className='progress'>
                  <div className='progress-bar' style={{ width: '75%' }}><span className='block' /></div>
                </div>
              </div>
              <div className='col-md-12 col-sm-12 col-xs-12'>
                <div className='text-center'>
                  <div className='stats white-text'><h4>32</h4>Thành Viên</div>
                </div>
                <div className='donate-button text-center'>
                  <a className='btn btn-donate' href='#'><i className='fa fa-support' /> Tham Gia Ngay</a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
