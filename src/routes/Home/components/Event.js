import React, { Component } from 'react'
export class Event extends Component {
  render () {
    const { praiseCurrent } = this.props
    console.log(praiseCurrent, 'praise of event')
    return (
      <div>
        <section id='event-header' className='event-header'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-12 col-sm-12 col-xs-12'>
                <h2 className='lg-title text-center'><span>Sự kiện sắp tới</span></h2>
              </div>
              <div className='col-md-8 col-sm-12 col-xs-12 col-md-offset-2'>
                <p className='lg-text text-center'>Là nơi thông báo về những sự kiện sẽ được tổ chức trong thời gian sắp tới
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id='event' className='event'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-7'>
                <i className='fa fa-calendar fa-3x pull-left event-icon' />
                <h3 className='event-title'>Hội trại hè 2018</h3>
              </div>
              <div className='col-md-5'>
                <div className='countdown-wrapper'>
                  <ul id='countdown'>
                    <li>
                      <span className='days'>54</span>
                      <p>Ngày</p>
                    </li>
                    <li>
                      <span className='hours'>21</span>
                      <p>Giờ </p>
                    </li>
                    <li>
                      <span className='minutes'>17</span>
                      <p>Phút</p>
                    </li>
                    <li>
                      <span className='seconds'>25</span>
                      <p>Giây</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id='volunteer-section' className='volunteer-section'>
          <div className='volunteer-header'>
            <div className='container'>
              <div className='row'>
                <div className='col-md-12 col-sm-12 col-xs-12'>
                  <h2 className='lg-title text-center'><span>chi đoàn viên tiêu biểu</span></h2>
                </div>
                <div className='col-md-6 col-sm-12 col-xs-12 col-md-offset-3'>
                  <p className='lg-text text-center'>Tuyên dương, vinh danh những bí thư chi đoàn giỏi và bí thư chi đoàn xuất sắc tiêu biểu</p>
                </div>
              </div>
            </div>
          </div>
          <div className='volunteers pad-b-80'>
            <div className='container'>
              <div className='row'>
                {
                  praiseCurrent && praiseCurrent.map((item, index) => (
                    <div className='col-md-3 col-sm-6 col-xs-12'>
                      <div className='team-wrap text-center'>
                        <div className='team-img'>
                          <img className='img-responsive' style={{ height:200, width:'100%' }} src={item.images[0].url}
                               alt='team-avatar' />
                          <div className='team-social'>
                            <a href='#'><i className='fa fa-twitter' /></a>
                            <a href='#'><i className='fa fa-facebook' /></a>
                            <a href='#'><i className='fa fa-pinterest' /></a>
                          </div>
                        </div>
                        <div className='team-content'>
                          <h4>{item.name}</h4>
                          <span className='designation'>{item.content}</span>
                          <p />
                        </div>
                      </div>

                    </div>

                  ))
                }
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}



