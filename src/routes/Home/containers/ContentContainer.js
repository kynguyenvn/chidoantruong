import { connect } from 'react-redux'
import { fetchGallery, fetchAboutus, fetchPraise } from '../modules/Content'
import ContentView from '../components/Home'

const mapDispatchToProps = {
  fetchGallery,
  fetchAboutus,
  fetchPraise
}

const mapStateToProps = (state) => {
  return ({
    listGallery : state.Content.listGallery,
    listAboutus : state.Content.listAboutus,
    listPraise  : state.Content.listPraise

  })
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentView)
