import { connect } from 'react-redux'
import { submitProperty, uploadFile } from '../modules/addAlbum'

import AddAlbumView from '../components/AddAlbumView'

const mapDispatchToProps = {
  submitProperty,
  uploadFile
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbumView)
