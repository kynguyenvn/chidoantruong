import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
class AddAlbumView extends Component {
  constructor () {
    super()
    this.state = {
      isLoading: false,
      imagesPreviewUrl: [],
      files: [],
      name: '',
      content:'',
      value:'gallery',
    }
  };

  _handleImageChange = async (e) => {
    e.preventDefault()
    const imagesPreviewUrl = []
    const files = e.target.files
    const countFiles = e.target.files.length
    for (let i = 0; i < countFiles; i++) {
      const reader = new FileReader()
      reader.onload = (e) => {
        imagesPreviewUrl.push({
          url: e.target.result,
          name: files[i].name,
          size: files[i].size
        })
        if (i === countFiles - 1) {
          this.setState({ imagesPreviewUrl, files })
        }
      }
      reader.readAsDataURL(e.target.files[i])
    }
  }
  onChangeLocation = (value, location) => {
    // console.log(value[0], value[0].formatted_address)
    this.setState({ location: location, address: value[0].formatted_address })
  }
  bytesToSize = (bytes) => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    if (bytes === 0) return '0 Byte'
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i]
  }
  takeInput () {
    const text = this.refs.name.value
    console.log(text)
  }
  render () {
    let { imagesPreviewUrl } = this.state

    return (
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='card'>
              <div className='card-body'>
                <form className='pro-add-form' style={{ paddingTop: 40, paddingBottom: 40 }}>
                  <div className='row'>
                    <div className='col-md-3' />
                    <div className='col-md-6'>
                      <div className='form-group'>
                        <label htmlFor='name'>Loại nội dung </label>
                        <select value={this.state.value} onChange={(e) => (this.setState({ value:e.target.value }))} style={{ border: '1px solid #5d5d5d', padding: 5, width: 200, marginLeft: 50, background: 'white' }}>
                          <option value='gallery'>Thư viện ảnh</option>
                          <option value='aboutUs'>Về chi đoàn</option>
                          <option value='praise'>Đoàn viên tiêu biểu</option>
                        </select>
                      </div>
                      <div className='form-group'>
                        <label htmlFor='name'>Tên</label>
                        <input type='text' value={this.state.name} onChange={(e) => (this.setState({ name: e.target.value }))} className='form-control' id='name' placeholder='' />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='name'>Nội dung</label>
                        <textarea value={this.state.content} onChange={(e) => (this.setState({ content: e.target.value }))} />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='input-file-now'>Tải ảnh lên</label>
                        <input type='file' ref='file' multiple onChange={(e) => this._handleImageChange(e)} id='input-file-now' className='dropify' />
                        <div className='table-responsive'>
                          <table className='table table-striped'>
                            <tbody>
                              {
                              imagesPreviewUrl.map((item, index) => (
                                <tr className='' key={index}>
                                  <td>
                                    <img src={item.url} alt='' />
                                  </td>
                                  <td>
                                    <p className='name'>{item.name}</p>
                                    <strong className='error text-danger' />
                                  </td>
                                  <td>
                                    <p className='size'>{this.bytesToSize(item.size)}</p>
                                    <div className='progress progress-striped active' role='progressbar' aria-valuemin='0' aria-valuemax='100' aria-valuenow='0'>
                                      <div className='progress-bar progress-bar-success' />
                                    </div>
                                  </td>
                                  <td>
                                    <button className='btn btn-primary start m-t-10'>
                                      <i className='fa fa-fw ti-export' />
                                      <span>Bắt đầu</span>
                                    </button>

                                    <button className='btn btn-warning cancel m-t-10'>
                                      <i className='fa fa-fw ti-na' />
                                      <span>Hủy</span>
                                    </button>

                                  </td>
                                </tr>
                              ))
                            }

                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className='text-center'>
                        <button type='button' onClick={() => { this.props.submitProperty(this.state) }} disabled={this.state.isLoading} className='btn btn-success waves-effect waves-light m-r-10'>
                          Tạo Album
                          {
                            this.state.isLoading && <i className='fa fa-spinner fa-spin' style={{ paddingLeft: 5 }} />
                          }
                        </button>
                        <Link to='/'> <button type='button' className='btn btn-dark waves-effect waves-light' style={{ marginLeft: 10 }}>Hủy</button></Link>

                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>)
  }
}

AddAlbumView.propTypes = {
  submitProperty: PropTypes.func.isRequired,
  uploadFile: PropTypes.func.isRequired,
}
export default AddAlbumView
