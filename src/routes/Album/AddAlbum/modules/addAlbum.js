// Constants
import { browserHistory } from 'react-router'
import firebase from 'firebase'
import moment from 'moment'
// Actions
export const submitProperty = (state, cb) => {
  // event.preventDefault()
  return (dispatch) => {
    uploadFile(state.files, (images) => {
      if (images && images.length > 0) {
        const date = moment().locale('vi').format('LLL')
        console.log(date, 'dm day day ne')
        const id = firebase.database().ref().child(`${state.value}`).push().key
        const data = {
          name : state.name,
          content:state.content,
          id : id,
          images,
          dateCreate: date,

        }

        const updates = {}
        updates[`/${state.value}/` + id] = data
        return firebase.database().ref().update(updates).then(() => {
          console.log('add success')
          browserHistory.push('/')
        })
      }
    })
  }
}
export const uploadFile = async (files, cb) => {
  const images = []
  console.log(files)
  this.files = files
  const storageRef = firebase.storage().ref()

  for (let i = 0; i < files.length; i++) {
    const file = this.files[i]

// Create the file metadata
    const metadata = {
      contentType: 'image/jpeg'
    }

// Upload file and metadata to the object 'images/mountains.jpg'
    await storageRef.child('images/' + file.name).put(file, metadata).then(await function (result) {
      images.push({
        url: result.downloadURL
      })
      if (i === files.length - 1) {
        cb(images)
      }
    }).catch((e) => {
    })
  }
}

export const actions = {
  submitProperty,
  uploadFile
}
