// ------------------------------------
// Constants
// ------------------------------------
import { browserHistory } from 'react-router'
import axios from 'axios'
import { apiUrl } from '../../../../api/index'
import { addToast } from '../../../../store/toasts'
import firebase from 'firebase'

// ------------------------------------
// Actions
// ------------------------------------

const handleFilesImage = (files) => {
  const array = []
  for (let i = 0; i < files.length; i++) {
    array.push({
      url: `${apiUrl}containers/container1/download/${files[i].name}`,
      name: files[i].name,
      size: files[i].size
    })
  }
  return array
}
export const submitProperty = (state, cb) => {
  if (state.files.length > 0) {
    uploadFile(state.files, (images) => {
      if (images && images.length > 0) {
        const data = {
          name: state.name,
          content: state.content,
          id: state.id,
          images,
          dateCreate: state.dateCreate,
          dateEdit: new Date()

        }

        const updates = {}
        updates[`/${state.value}/` + state.id] = data
        return firebase.database().ref().update(updates).then(() => {
          console.log('add success')
          browserHistory.push('/')
        })
      }
    })
  } else {
    const data = {
      name: state.name,
      content: state.content,
      id: state.id,
      dateCreate: state.dateCreate,
      dateEdit: new Date()
    }

    const updates = {}
    updates[`/${state.value}/` + state.id] = data
    return firebase.database().ref().update(updates).then(() => {
      console.log('add success')
      browserHistory.push('/')
    })
  }
}

export const uploadFile = (files, cb) => {
  const images = []
  console.log(files)
  this.files = files
  const storageRef = firebase.storage().ref()

  for (let i = 0; i < files.length; i++) {
    const file = this.files[i]

// Create the file metadata
    const metadata = {
      contentType: 'image/jpeg'
    }

// Upload file and metadata to the object 'images/mountains.jpg'
    storageRef.child('images/' + file.name).put(file, metadata).then(function (result) {
      console.log(result)
      images.push({
        url: result.downloadURL
      })
      if (i === files.length - 1) {
        cb(images)
      }
    }).catch((e) => {
      console.log(e)
    })
  }
}

export const actions = {
  submitProperty,
  uploadFile
}
