import { connect } from 'react-redux'
import { submitProperty, uploadFile } from '../modules/editAlbum'

import EditAlbumView from '../components/EditAlbumView'

const mapDispatchToProps = {
  submitProperty,
  uploadFile
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(EditAlbumView)
