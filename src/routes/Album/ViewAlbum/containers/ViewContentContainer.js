import { connect } from 'react-redux'
import { submitProperty, uploadFile } from '../modules/viewcontent'


import ViewContentView from '../components/ViewContentView'

const mapDispatchToProps = {
  submitProperty,
  uploadFile
}

const mapStateToProps = (state) => ({

})


export default connect(mapStateToProps, mapDispatchToProps)(ViewContentView)
