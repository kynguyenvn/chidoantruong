import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './RealEstateAddView.scss'

class ViewContentView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: false,
      imagesPreviewUrl: props.location.state.albumData.images,
      files: [],
      name: props.location.state.albumData.name,
      content:props.location.state.albumData.content,
      value:'gallery',
    }
  };

  onChangeLocation = (value, location) => {
    // console.log(value[0], value[0].formatted_address)
    this.setState({ location: location, address: value[0].formatted_address })
  }
  
  render () {
    let { imagesPreviewUrl } = this.state

    return (
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='card'>
              <div className='card-body'>
                <form className='pro-add-form'>
                  <div className='row'>
                    <div className='col-md-6'>
                      <div className='form-group'>
                        <label htmlFor='name'>Property Name</label>
                        <select value={this.state.value} onChange={(e) => (this.setState({ value:e.target.value }))}>
                          <option value='gallery'>gallery</option>
                          <option value='aboutUs'>aboutUs</option>
                        </select>
                      </div>
                      <div className='form-group'>
                        <label htmlFor='name'>Property Name</label>
                        <input type='text' value={this.state.name} onChange={(e) => (this.setState({ name: e.target.value }))} className='form-control' id='name' placeholder='Enter Name' />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='name'>Content</label>
                        <textarea value={this.state.content} onChange={(e) => (this.setState({ content: e.target.value }))} />
                      </div>
                    </div>
                  </div>
                  <div className='form-group'>
                    <label htmlFor='input-file-now'>Upload Files</label>
                    <input type='file' ref='file' multiple onChange={(e) => this._handleImageChange(e)} id='input-file-now' className='dropify' />
                    <div className='table-responsive'>
                      <table className='table table-striped'>
                        <tbody>
                          {
                          imagesPreviewUrl.map((item, index) => (
                            <tr className='' key={index}>
                              <td>
                                <img src={item.url} alt='' />
                              </td>
                              <td>
                                <p className='name'>{item.name}</p>
                                <strong className='error text-danger' />
                              </td>
                              <td>
                                <p className='size'></p>
                                <div className='progress progress-striped active' role='progressbar' aria-valuemin='0' aria-valuemax='100' aria-valuenow='0'>
                                  <div className='progress-bar progress-bar-success' />
                                </div>
                              </td>
                              <td>
                                <button className='btn btn-primary start m-t-10'>
                                  <i className='fa fa-fw ti-export' />
                                  <span>Start</span>
                                </button>

                                <button className='btn btn-warning cancel m-t-10'>
                                  <i className='fa fa-fw ti-na' />
                                  <span>Cancel</span>
                                </button>

                              </td>
                            </tr>
                          ))
                        }

                        </tbody>
                      </table>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>)
  }
}

ViewContentView.propTypes = {
  submitProperty: PropTypes.func.isRequired,
  uploadFile: PropTypes.func.isRequired,
}
export default ViewContentView
