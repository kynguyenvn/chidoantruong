// ------------------------------------
// Constants
// ------------------------------------
import { browserHistory } from 'react-router'
import axios from 'axios'
import { apiUrl } from '../../../../api/index'
import { addToast } from '../../../../store/toasts'
import firebase from 'firebase'

// ------------------------------------
// Actions
// ------------------------------------

const handleFilesImage = (files) => {
  const array = []
  for (let i = 0; i < files.length; i++) {
    array.push({
      url: `${apiUrl}containers/container1/download/${files[i].name}`,
      name: files[i].name,
      size: files[i].size
    })
  }
  return array
}
export const deleteDatabase = () => {
  return(dispatch) => {
    const commentsRef = firebase.database().ref('post-comments/' + postId);
  }
}
export const submitProperty = (state, cb) => {
  // event.preventDefault()
  console.log(state,'state day ne')
  return (dispatch) => {
    uploadFile(state.files, (images) => {
      if (images && images.length > 0) {
        const id = firebase.database().ref().child(`${state.value}`).push().key
        const data = {
          name : state.name,
          content:state.content,
          id : id,
          images,
          dateCreate: new Date().toLocaleTimeString(),
        }

        const updates = {}
        updates[`/${state.value}/` + id] = data
        return firebase.database().ref().update(updates).then(() => {
          console.log('add success')
          browserHistory.push('/')
        })
      }
    })
  }
}
export const uploadFile = (files, cb) => {
  const images = []
  console.log(files)
  this.files = files
  const storageRef = firebase.storage().ref()

  for (let i = 0; i < files.length; i++) {
    const file = this.files[i]

// Create the file metadata
    const metadata = {
      contentType: 'image/jpeg'
    }

// Upload file and metadata to the object 'images/mountains.jpg'
    storageRef.child('images/' + file.name).put(file, metadata).then(function (result) {
      console.log(result)
      images.push({
        url: result.downloadURL
      })
      if (i === files.length - 1) {
        cb(images)
      }
    }).catch((e) => {
      console.log(e)
    })
  }
}

export const actions = {
  submitProperty,
  uploadFile
}
