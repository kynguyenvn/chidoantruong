// Constants
import firebase from 'firebase/index'

export const FETCH_GALLERY_SUCCESS = 'FETCH_GALLERY_SUCCESS'
export const FETCH_ABOUTUS_SUCCESS = 'FETCH_ABOUTUS_SUCCESS'

// Actions

const fetchGallerySuccess = (listGallery) => ({
  type    : FETCH_GALLERY_SUCCESS,
  listGallery
})
const fetchAboutusSuccess = (listAboutus) => ({
  type    : FETCH_ABOUTUS_SUCCESS,
  listAboutus
})

export const deleteAlbum = (id) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const updates = {}
      updates[`gallery/` + id] = null
      return firebase.database().ref().update(updates).then(() => {
        console.log('delete success')
      })
    })
  }
}
export const fetchGallery = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/gallery/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        console.log(Object.values(data))
        if (cb)cb(Object.values(data))
        dispatch(fetchGallerySuccess(Object.values(data)))
      })
    })
  }
}
export const fetchAboutus = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/aboutUs/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        if (cb)cb(Object.values(data))
        dispatch(fetchAboutusSuccess(Object.values(data)))
      })
    })
  }
}

export const actions = {
  fetchGallery, fetchAboutus
}

// Action Handlers

const ACTION_HANDLERS = {
  [FETCH_GALLERY_SUCCESS] : (state, action) => ({ ...state, listGallery: action.listGallery }),
  [FETCH_ABOUTUS_SUCCESS] : (state, action) => ({ ...state, listAboutus: action.listAboutus })
}

// Reducer

const initialState = {
  listGallery: [],
  listAboutus:[]
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
