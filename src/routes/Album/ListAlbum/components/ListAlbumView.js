import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactTable from 'react-table'
import { Link } from 'react-router'

class ListAlbumView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      galleriesCurrent: [],
      gallery: {},
      arrayAlbum:[],
      aboutusCurrent: [],
      aboutUs : {},
    }
  }

  componentWillMount () {
    // fetch gallery
    this.props.fetchGallery((galleries) => {
      const galleriesCurrent = galleries.filter(item => item.name).reverse()
      this.setState({
        galleriesCurrent,
        gallery: galleriesCurrent[0]
      })
    })
  }

  render () {
    const { galleriesCurrent, } = this.state
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-2 '></div>
          <div className='col-md-8' style={{ paddingTop:20 }}>
            <ReactTable
              data={galleriesCurrent}
              columns={[
                {
                  Header: 'Tên',
                  columns: [
                    {
                      Header: 'Tên Album',
                      accessor: 'name',
                      maxWidth: 400
                    }
                  ]
                },

                {
                  Header: 'Thông Tin',
                  columns: [
                    {
                      Header: 'Nội Dung',
                      id: 'orderStatus',
                      maxWidth: 400,
                      accessor:'content'
                    },
                    {
                      Header: 'Ngày Tạo',
                      accessor: 'dateCreate',
                      maxWidth: 200,
                      sort: 'desc',
                      render: props => <span>{props.dateCreate}</span>

                    }
                  ]
                },

                {
                  Header: 'Hành động',
                  columns: [
                    {
                      Header: 'Chỉnh sửa nội dung',
                      id: 'action',
                      accessor: data => {
                        return (<div className='box-actions text-center'>

                          <Link to={{ pathname: `/edit-album/${data.id}`, state:{ albumData: data } }} className='btn btn-default btn-sm'>
                            Sửa
                          </Link>

                          <Link to={{ pathname: `/view/${data.id}`, state:{ albumData: data } }} className='btn btn-default btn-sm'>
                            Xem
                          </Link>
                          <Link onClick={() => this.props.deleteAlbum(data.id)} className='btn btn-default btn-sm'>
                            Xóa
                          </Link>
                        </div>)
                      },
                    },
                  ]
                },
              ]}
              defaultSorted={[
                {
                  id: 'no',
                  desc: false
                }
              ]}
              defaultPageSize={10}
              className='-striped -highlight'
            />
          </div>
        </div>
      </div>
    )
  }
}
ListAlbumView.propTypes = {
  fetchGallery: PropTypes.func.isRequired,
  listGallery: PropTypes.array.isRequired,
  fetchAboutus: PropTypes.func.isRequired,
  listAboutus: PropTypes.array.isRequired,
  deleteAlbum: PropTypes.func.isRequired,
}
export default ListAlbumView
