import { connect } from 'react-redux'
import { fetchGallery, fetchAboutus, deleteAlbum } from '../modules/listAlbum'
import ListAlbumView from '../components/ListAlbumView'

const mapDispatchToProps = {
  fetchGallery,
  fetchAboutus,
  deleteAlbum
}

const mapStateToProps = (state) => {
  return ({
    listGallery : state.listAlbum.listGallery,
    listAboutus : state.listAlbum.listAboutus,

  })
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAlbumView)
