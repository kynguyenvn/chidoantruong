import { connect } from 'react-redux'

import DetailAlbumView from '../components/DetailAlbumView'

const mapDispatchToProps = {
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(DetailAlbumView)
