import React, { Component } from 'react'
import PropTypes from 'prop-types'

class DetailAlbumView extends Component {
  constructor (props) {
    super(props)
    console.log(props, 'props cua detail')
    this.state = {
      albumData: props.location.state.albumData,
      id: props.location.state.albumData.id,
      dateCreate: props.location.state.albumData.dateCreate,
      name: props.location.state.albumData.name,
      content:props.location.state.albumData.content,
    }
  }
  render () {
    let { albumData } = this.state
    console.log(albumData, 'olala')
    return (
      <div className='container-fluid'>
        <section id='causes' className='causes'>
          <div className='container'>
            <div className='row text-center'>
              <div className='isotope-nav' />
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='row'>
                  <div className='urgent-causes' style={{ position: 'relative', height: '491.5px' }}>
                    <div className='row'>
                      <h3 className='text-success'>{albumData.name}</h3>

                      {
                        albumData.images.map((item, index) => (
                          <div key={index} className='col-md-4'>
                            <img style={{ height:250, width:'100%', padding: 5 }} src={item.url} alt='' />
                          </div>
                          )
                        )
                      }

                    </div>
                    <p>{albumData.content}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className='row text-center' />
          </div>
        </section>
      </div>
    )
  }
}

DetailAlbumView.propTypes = {
}
export default DetailAlbumView
