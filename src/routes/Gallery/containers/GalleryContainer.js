import { connect } from 'react-redux'
import { fetchGallery } from '../modules/Gallery'

import GalleryView from '../components/GalleryView'

const mapDispatchToProps = {
  fetchGallery
}

const mapStateToProps = (state) => {
  return ({
    listGallery: state.gallery.listGallery
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryView)
