import firebase from 'firebase/index'

// Constants
export const FETCH_GALLERY_SUCCESS = 'FETCH_GALLERY_SUCCESS'

// Actions

const fetchGallerySuccess = (listGallery) => ({
  type: FETCH_GALLERY_SUCCESS,
  listGallery
})
export const fetchGallery = () => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/gallery/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        dispatch(fetchGallerySuccess(Object.values(data)))
      })
    })
  }
}


export const actions = {
  fetchGallery
}

// Action Handlers

const ACTION_HANDLERS = {
  [FETCH_GALLERY_SUCCESS]    : (state, action) => ({ ...state, listGallery: action.listGallery })
}

// Reducer

const initialState = {
  listGallery : []
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
