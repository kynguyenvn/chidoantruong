import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
export class GalleryView extends Component {
  componentWillMount () {
    this.props.fetchGallery()
  }
  render () {
    console.log(this.props.listGallery)
    return (
      <div>
        <section id='nav-helper' className='nav-helper bg-image' style={{ backgroundImage: `url(${require('../../../components/assets/masshead-2.jpg')})` }}>
          <div className='container'>
            <div className='row'>
              <div className='col-md-5'>
                <div className='page-head'>
                  <i className='fa fa-image' />
                  <h2>Thư Viện Ảnh</h2>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id='main-content' className='main-content pad-60'>
          <div className='container'>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='row'>
                  {
                    this.props.listGallery.map((item, index)=> (
                      <div>
                        <div key={index} className='col-md-3' >
                          <img src= { item.images[0].url} style={{ width: '100%'}} />
                        </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
GalleryView.propTypes = {
  fetchGallery: PropTypes.func.isRequired,
  listGallery: PropTypes.array.isRequired
}
export default GalleryView
