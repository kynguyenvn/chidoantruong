import { connect } from 'react-redux'
import { login, logout } from '../modules/login'

import LoginView from '../components/LoginView'

const mapDispatchToProps = {
  login, logout
}

const mapStateToProps = (state) => {
  return ({
    isLogin: state.login.isLogin
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView)
