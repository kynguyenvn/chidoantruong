import React, { Component } from 'react'
import './LoginView.scss'
import PropTypes from 'prop-types'
class LoginView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email:'nguyen@gmail.com',
      password:'123456'
    }
    this.onSubmitLogin = this.onSubmitLogin.bind(this)
  }
  onSubmitLogin (e) {
    e.preventDefault()
    this.props.login(this.state)
  }
  tryLogout () {
    this.props.logout()
  }
  render () {
    return (
      <div className='login'>
        <div className='login-triangle' />
        <h2 className='login-header'>Đăng Nhập</h2>
        <form onSubmit={(e) => this.onSubmitLogin(e)} className='login-container'>
          <input
            type='email'
            value={this.state.email}
            onChange={(e) => (this.setState({ email:e.target.value }))}
            placeholder='Email'
          />
          <input
            type='password'
            value={this.state.password}
            onChange={(e) => (this.setState({ password:e.target.value }))}
            placeholder='Password'
          />
          <input type='submit' value='Log in' />
        </form>
      </div>
    )
  }
 }
LoginView.propTypes = {
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
}
export default LoginView
