import firebase from 'firebase'
import { browserHistory } from 'react-router'

// Constants

export const LOAD_LOGIN_SUCCESS = 'LOAD_LOGIN_SUCCESS'
export const LOAD_LOGOUT_SUCCESS = 'LOAD_LOGOUT_SUCCESS'

// Actions

const loadLoginSuccess = (isLogin) => ({
  type: LOAD_LOGIN_SUCCESS,
  isLogin: true
})
const loadLogoutSuccess = (isLogin) => ({
  type: LOAD_LOGOUT_SUCCESS,
  isLogin: false
})

export function login (state) {
  return (dispatch, getState) => {
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(function () {
        return firebase.auth().signInWithEmailAndPassword(state.email, state.password)
          .then(res => {
            dispatch(loadLoginSuccess())
            browserHistory.push('/')
          })
          .catch(function (error) {
            const errorCode = error.code
            const errorMessage = error.message
            console.log(errorCode, errorMessage)
          })
      })
  }
}
export function logout () {
  return (dispatch) => {
    console.log(1)
    firebase.auth().signOut().then(function () {
      dispatch(loadLogoutSuccess())
      console.log('logout success')
    // Sign-out successful.
    }).catch(function (error) {
    // An error happened.
      console.log(error)
    })
  }
}

export const checkLogin = () => {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged(function (user) {
      window.user = user
      if (user) {
        dispatch(loadLoginSuccess())
      }
    })
  }
}

export const actions = {
  login, logout
}

// Action Handlers

const ACTION_HANDLERS = {
  [LOAD_LOGIN_SUCCESS]    : (state, action) => ({ ...state, isLogin: action.isLogin }),
  [LOAD_LOGOUT_SUCCESS]    : (state, action) => ({ ...state, isLogin: action.isLogin })
}

// Reducer

const initialState = {
  isLogin : false
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
