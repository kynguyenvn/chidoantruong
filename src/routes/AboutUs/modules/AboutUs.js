// Constants
import firebase from 'firebase/index'

export const FETCH_ABOUTUS_SUCCESS = 'FETCH_ABOUTUS_SUCCESS'

// Actions

const fetchAboutusSuccess = (listAboutus) => ({
  type    : FETCH_ABOUTUS_SUCCESS,
  listAboutus
})

export const fetchAboutus = (cb) => {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      const ref = firebase.database().ref('/aboutUs/')
      ref.on('value', snapshot => {
        const data = snapshot.val()
        if (cb)cb(Object.values(data))
        dispatch(fetchAboutusSuccess(Object.values(data)))
      })
    })
  }
}
export const actions = {
  fetchAboutus
}

// Action Handlers

const ACTION_HANDLERS = {
  [FETCH_ABOUTUS_SUCCESS] : (state, action) => ({ ...state, listAboutus: action.listAboutus }),
}

// Reducer

const initialState = {
  listAboutus:[]
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
