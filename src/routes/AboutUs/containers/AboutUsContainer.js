import { connect } from 'react-redux'
import { fetchAboutus } from '../modules/AboutUs'
import AboutUsView from '../components/AboutUsView'

const mapDispatchToProps = {
  fetchAboutus
}

const mapStateToProps = (state) => {
  return ({
    listAboutus : state.aboutus.listAboutus

  })
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutUsView)
