import React, { Component } from 'react'
import Carousel1 from '../../../components/Carousel1'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

export class AboutUsView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      aboutUsCurrent:[],
      aboutUs:{}
    }
  }
  componentWillMount () {
    this.props.fetchAboutus((aboutUss) => {
      const aboutUsCurrent = this.handleAboutUs(aboutUss)
      aboutUsCurrent.sort(function compare (a, b) {
        const dateA = new Date(a.dateCreate)
        const dateB = new Date(b.dateCreate)
        return dateB - dateA
      })
      this.setState({
        aboutUsCurrent,
        aboutUs: aboutUsCurrent[0]
      })
      console.log(aboutUsCurrent, 'data of abouUs view')
    })
  }
  handleAboutUs (aboutUss) {
    const lengAboutUs = aboutUss.length
    let array = []
    for (let i = 0; i < lengAboutUs; i++) {
      if (i > lengAboutUs - 5) {
        array.push(aboutUss[i])
      }
    }
    return array
  }
  render () {
    const { aboutUsCurrent } = this.state
    return (
      <div>
        <section id='nav-helper' className='nav-helper bg-image' style={{ backgroundImage: `url(${require('../../../components/assets/masshead-2.jpg')})` }}>
          <div className='container'>
            <div className='row'>
              <div className='col-md-5'>
                <div className='page-head'>
                  <i className='fa fa-briefcase' />
                  <h2>Chi Đoàn Ngô Thì Nhậm</h2>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id='main-content' className='main-content pad-60'>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-6 col-xs-12'>
                <div className='article'>
                  <h3>Chào mừng tới chi đoàn Ngô Thì Nhậm</h3>
                  <p className='text-justify'>Thành lập năm 1977 với tên gọi ban đầu là trường vừa học vừa làm Thanh Trì, trải qua 40 năm xây dựng và trưởng thành thầy và trò nhà trường đã đạt được những kết quả đáng khích lệ: 38 năm liên tục đạt danh hiệu tập thể lao động Tiên tiến và Tập thể lao động Xuất sắc; Chi bộ trong sạch vững mạnh và vững mạnh tiêu biểu; Công đoàn liên tục được công nhận là Công đoàn vững mạnh và vững mạnh xuất sắc; Đoàn thanh niên nhiều năm được Trung ương đoàn và Thành đoàn tặng Bằng khen.</p>
                </div>
              </div>
              <div className='col-sm-6 col-xs-12'>
                <div id='about-slider' className='about-slider owl-carousel owl-theme owl-loaded'>
                  <Carousel1 aboutUsCurrent={aboutUsCurrent} />
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    )
  }
}
AboutUsView.propTypes = {
  fetchAboutus: PropTypes.func.isRequired,
  listAboutus : PropTypes.array.isRequired
}

export default AboutUsView
