import CoreLayout from '../layouts/PageLayout/PageLayout'
import AddAlbumRoute from './Album/AddAlbum'
import ContentRoute from './Home'
import AboutUsRoute from './AboutUs'
import GalleryRoute from './Gallery'
import LoginRoute from './Login'
import EditContentRoute from './Album/EditAlbum'
import ListContentRoute from './Album/ListAlbum'
import ViewContentRoute from './Album/ViewAlbum'
import DetailAlbumRoute from './Album/DetailAlbum'

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  :ContentRoute(store),
  childRoutes : [
    ContentRoute(store),
    AboutUsRoute(store),
    GalleryRoute(store),
    LoginRoute(store),
    ListContentRoute(store),
    EditContentRoute(store),
    AddAlbumRoute(store),
    ViewContentRoute(store),
    DetailAlbumRoute(store),

  ]
}
)

export default createRoutes
