import { combineReducers } from 'redux'
import siteReducer from './site'
import toasts from './toasts'
import auth from '../routes/Login/modules/login'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    site: siteReducer,
    toasts,
    auth,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
