import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkLogin, logout } from '../../routes/Login/modules/login'
import PropTypes from 'prop-types'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import Toasts from '../../components/Toast/Toasts'
import './PageLayout.scss'

class PageLayout extends Component {
  componentWillMount () {
    this.props.checkLogin()
  }
  render () {
    const { children } = this.props
    return (
      <div id='main-wrapper'>
        <Header logout={() => this.props.logout()} isLogin={this.props.isLogin} />
        {children}
        <Toasts />
        <Footer />
      </div>
    )
  }
}
PageLayout.propTypes = {
  children: PropTypes.node,
  checkLogin: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  isLogin: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
  isLogin: state.auth.isLogin
})
const mapDispatchToProps = {
  checkLogin,
  logout
}

export default connect(mapStateToProps, mapDispatchToProps)(PageLayout)
